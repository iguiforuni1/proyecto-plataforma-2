using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class efoctosonido : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] private AudioClip colector1;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        audioSource.Play();
    }
}
