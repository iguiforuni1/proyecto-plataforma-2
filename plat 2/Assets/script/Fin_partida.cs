using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fin_partida : MonoBehaviour
{
    private GameObject _gameObject;
    private Rigidbody2D _rb;
    public GameObject menu_victoria;

    private bool isterminado = false;   
    
    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _gameObject = GameObject.FindGameObjectWithTag("Jugador");
        menu_victoria.SetActive(false);    
    }

    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Fin"))
        {
            isterminado = true; 
            Time.timeScale = 0f;
            menu_victoria.SetActive(true);
        }
       
    }
}
