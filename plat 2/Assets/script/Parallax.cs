using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    [SerializeField] private Vector2 velocidadMovimiento;
    private Vector2 offset;
    private Renderer renderizador;
    private Rigidbody2D jugadorRB;

    private void Awake()
    {
        renderizador = GetComponent<Renderer>();
        jugadorRB = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Rigidbody2D>(); 
    }

    private void Update()
    {
        offset = (jugadorRB.velocity.x / 10 * 0.1f) * velocidadMovimiento * Time.deltaTime; 
        renderizador.material.mainTextureOffset += offset;

    }
}