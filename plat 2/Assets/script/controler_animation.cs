using System.Collections;
using System.Collections.Generic;
using TarodevController;
using UnityEngine;

public class controler_animation : MonoBehaviour
{
    private Animator anim;
    private PlayerController _player;

    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        _player = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Horizontal") != 0 /*&& _player._grounded*/)
        {
            anim.SetBool("move", true);
        }
        else 
        {
            anim.SetBool("move", false);

        }
    }
}
