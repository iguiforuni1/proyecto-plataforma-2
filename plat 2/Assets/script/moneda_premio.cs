using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moneda_premio : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] private AudioClip colector1;

   
    private void OnTriggerEnter2D(Collider2D other)
    {


        if (other.CompareTag("Jugador"))
        {
            controladoraudio.Instance.EjecutarSonido(colector1);
            gameObject.SetActive(false);
        }
    }
    
}
