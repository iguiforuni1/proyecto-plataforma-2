using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigo_code : MonoBehaviour
{
    public Transform puntoCentral;  // El punto alrededor del cual la entidad se mover�
    public float radio = 5.0f;      // El radio del c�rculo
    public float velocidadAngular = 30.0f; // La velocidad angular en grados por segundo

    private float angulo;  // El �ngulo actual en radianes
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        // Incrementar el �ngulo basado en la velocidad angular y el tiempo transcurrido
        angulo += velocidadAngular * Time.deltaTime;

        // Convertir el �ngulo a radianes
        float anguloRad = angulo * Mathf.Deg2Rad;

        // Calcular la nueva posici�n en el c�rculo
        float x = puntoCentral.position.x + radio * Mathf.Cos(anguloRad);
        float z = puntoCentral.position.z + radio * Mathf.Sin(anguloRad);

        // Calcular la nueva posici�n
        Vector3 nuevaPosicion = new Vector3(x, transform.position.y, z);

        // Actualizar la posici�n de la entidad
        transform.position = nuevaPosicion;

        // Voltear el sprite basado en el �ngulo
        if (Mathf.Cos(anguloRad) > 0)
        {
            spriteRenderer.flipX = false; // Mirando a la derecha
        }
        else
        {
            spriteRenderer.flipX = true; // Mirando a la izquierda
        }
    }
}