using UnityEngine;

public class SonidoMovimiento : MonoBehaviour
{
    public AudioSource audioSource; // Referencia al AudioSource donde reproducir el sonido
    [SerializeField] private AudioClip sonidoMovimiento; // AudioClip asignado desde el Inspector

    private Vector3 posicionAnterior; // Almacena la posici�n anterior del objeto en cada frame

    private void Start()
    {
        // Inicializa la posici�n anterior al inicio del juego
        posicionAnterior = transform.position;
    }

    private void Update()
    {
        // Comprueba si la posici�n actual del objeto es diferente de la posici�n anterior en el eje X
        if (transform.position.x != posicionAnterior.x)
        {
            // Si el objeto se ha movido en el eje X, reproduce el sonido de movimiento
            if (audioSource != null && sonidoMovimiento != null)
            {
                audioSource.PlayOneShot(sonidoMovimiento);
            }
        }

        // Actualiza la posici�n anterior del objeto para el siguiente frame
        posicionAnterior = transform.position;
    }
}